﻿using System;

namespace LerenWerkenMetKlassen
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            vulLijstMetProgrammeertaalNamen("Typ namen van programmeertalen in:", "De lijst is met de volgende programmeertalen ingevoerd:");
            Console.ReadKey();
        }

        static void vulLijstMetProgrammeertaalNamen(string startmessage, string endmessage)
        {
            Console.Write(startmessage);
            ProgrammeertaalNamen programmeertaalNamen = new ProgrammeertaalNamen();

            for (byte i = 0; i < 5; i++)
            {
                string programmeertaalNaam = Console.ReadLine();
                programmeertaalNamen.Add(programmeertaalNaam);
            }
            Console.WriteLine(endmessage);
            Console.WriteLine(programmeertaalNamen.printStringArray());

            Console.Write(startmessage);
            ProgrammeertaalNamen functioneleProgrammeertaalNamen = new ProgrammeertaalNamen();

            for(byte i = 0; i < 5; i++)
            {
                string programmeertaalNaam = Console.ReadLine();
                functioneleProgrammeertaalNamen.Add(programmeertaalNaam);
            }
            Console.WriteLine(endmessage);
            Console.WriteLine(functioneleProgrammeertaalNamen.printStringArray());
        }
    }
}